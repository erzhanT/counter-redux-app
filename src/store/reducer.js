import {
    ADD,
    DECREMENT,
    FETCH_COUNTER_FAILURE,
    FETCH_COUNTER_REQUEST,
    FETCH_COUNTER_SUCCESS,
    INCREMENT,
    SUBTRACT
} from "./actions";

const initialState = {
    counter: 0,
    loading: false,
    error: false,
};

const reducer = (state = initialState, action) => {

    switch (action.type) {
        case FETCH_COUNTER_REQUEST: {
            return {...state,  error: false}
        }
        case FETCH_COUNTER_SUCCESS: {
            return {...state, loading: false, counter: action.counter}
        }
        case FETCH_COUNTER_FAILURE: {
            return {...state, loading: false, error: true}
        }
        default:
            return state;
    }
};

export default reducer;